## lotterypick

[![build status](https://gitlab.com/johnjvester/lotterypick/badges/master/build.svg)](https://gitlab.com/johnjvester/lotterypick/commits/master)

> The lotterypick project is a sample application created to utilize [RandomGenerator](https://gitlab.com/johnjvester/RandomGenerator) in 
> order to try to pick the winning numbers from the 11/27/2016 Powerball - which had a $420.9 million payout.
>
> Running the main() method will perform 10,000,000 random picks (without duplicates) to try and match the following winning numbers:
>
> ```17-19-21-37-44 with a Powerball of 16```
>
> At the end, statistics are provided via usage of the  simple ```System.out.println()``` console output.

### Getting Started

In order to use this project, simply clone/download the project and Run the main() method.

#### Logging

There is a log4j2.xml file in this project, which is set to error-level logging.  It is possible to change the log levels, but doing so 
could consume significant disk space when performing 100,000 iterations (or more).

#### Additional Information

For additional information on RandomGenerator, please review the following JavaDoc:

[http://johnjvester.gitlab.io/RandomGenerator-JavaDoc/](http://johnjvester.gitlab.io/RandomGenerator-JavaDoc/)

Created by [John Vester](https://www.linkedin.com/in/johnjvester), because I truly enjoy writing code.